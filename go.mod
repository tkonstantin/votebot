module gitlab.com/tkonstantin/votebot

go 1.18

require (
	github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.5.1 // indirect
	github.com/google/uuid v1.3.0 // indirect
)
