package main

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.com/tkonstantin/votebot/internal"
	"log"
	"net/http"
	"os"
	"strconv"
	"sync"
)

func main() {
	api, err := tgbotapi.NewBotAPI(os.Getenv("TELEGRAM_APITOKEN"))
	if err != nil {
		log.Fatalf("can't init bot api: %v\n", err)
	}

	if os.Getenv("DEBUG") == "true" {
		api.Debug = true
	}

	port, err := strconv.Atoi(os.Getenv("HTTP_PORT"))
	if err != nil {
		port = 8080
	}

	log.Printf("Authorized on account %s", api.Self.UserName)

	userRepository := internal.NewMemoryUserRepository()
	pollRepository := internal.NewMemoryPollRepository()
	voteRepository := internal.NewMemoryVoteRepository()

	userFactory := internal.NewMemoryUserFactory()

	wg := sync.WaitGroup{}

	wg.Add(1)
	go func() {
		service := internal.NewVoteBotService(
			userFactory,
			userRepository,
			internal.NewMemoryPollFactory(),
			pollRepository,
			internal.NewMemoryVoteFactory(),
			voteRepository,
		)

		bot := internal.NewVoteBot(api, service)
		_ = bot.Run()
		wg.Done()
	}()

	wg.Add(1)
	go func() {
		service := internal.NewVoteApiService(
			pollRepository,
			voteRepository,
			userRepository,
			userFactory,
		)

		server := internal.NewVoteServer(service)
		_ = http.ListenAndServe(fmt.Sprintf(":%d", port), server)

		wg.Done()
	}()

	wg.Wait()
	log.Println("quitting...")
}
