package internal

import (
	"errors"
	"fmt"
	"sync"
)

type PollId string

type Poll struct {
	id       PollId
	userId   UserId
	title    string
	multiple bool
	options  []*Option
}

func newPoll(id PollId, userId UserId, title string, multiple bool, options []*Option) *Poll {
	return &Poll{
		id,
		userId,
		title,
		multiple,
		options,
	}
}

func (p *Poll) Id() PollId {
	return p.id
}

func (p *Poll) UserId() UserId {
	return p.userId
}

func (p *Poll) Options() []*Option {
	return p.options
}

func (p *Poll) Multiple() bool {
	return p.multiple
}

func (p *Poll) Title() string {
	return p.title
}

type Option struct {
	title string
}

func NewOption(title string) *Option {
	return &Option{
		title,
	}
}

func (o *Option) Title() string {
	return o.title
}

type uuidPollFactory struct{}

func NewMemoryPollFactory() PollFactory {
	return &uuidPollFactory{}
}

func (f *uuidPollFactory) CreatePoll(userId UserId, title string, multiple bool, options []*Option) (*Poll, error) {
	id, err := newId()
	if err != nil {
		return nil, fmt.Errorf("can't generate new UserId: %w", err)
	}

	p := newPoll(PollId(id), userId, title, multiple, options)
	return p, nil
}

type memoryPollRepository struct {
	rwMutex   sync.RWMutex
	polls     map[PollId]*Poll
	userPolls map[UserId][]PollId
}

func NewMemoryPollRepository() PollRepository {
	return &memoryPollRepository{
		polls:     make(map[PollId]*Poll),
		userPolls: make(map[UserId][]PollId),
	}
}

func (m *memoryPollRepository) Add(poll *Poll) error {
	m.rwMutex.Lock()
	defer m.rwMutex.Unlock()

	if _, ok := m.polls[poll.Id()]; ok {
		return errors.New("poll with this id already exists")
	}

	userPolls, ok := m.userPolls[poll.UserId()]
	if !ok {
		userPolls = []PollId{}
	}
	m.userPolls[poll.UserId()] = append(userPolls, poll.Id())

	m.polls[poll.Id()] = poll
	return nil
}

func (m *memoryPollRepository) Get(id PollId) (*Poll, error) {
	m.rwMutex.RLock()
	defer m.rwMutex.RUnlock()

	poll, ok := m.polls[id]
	if !ok {
		return nil, errors.New("cannot find poll with this id")
	}

	return poll, nil
}

func (m *memoryPollRepository) Update(id PollId, title string, multiple bool, options []*Option) error {
	m.rwMutex.Lock()
	defer m.rwMutex.Unlock()

	poll, ok := m.polls[id]
	if !ok {
		return errors.New("cannot find poll with this id")
	}

	m.polls[id] = newPoll(id, poll.UserId(), title, multiple, options)
	return nil
}

func (m *memoryPollRepository) GetUserPolls(userId UserId) ([]*Poll, error) {
	var polls []*Poll

	pollIds, ok := m.userPolls[userId]
	if !ok {
		return polls, nil
	}

	for _, pollId := range pollIds {
		poll, err := m.Get(pollId)
		if err != nil {
			return nil, fmt.Errorf("cannot get poll with this id: %w", err)
		}
		polls = append(polls, poll)
	}

	return polls, nil
}
