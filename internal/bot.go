package internal

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"log"
	"strings"
)

type ChatId int64

type VoteBotService interface {
	CreateUser(name string) (*User, error)
	CreatePoll(userId UserId, title string, multiple bool, options []*Option) (*Poll, error)
	GetPolls(userId UserId) ([]*Poll, error)
	UpdatePoll(pollId PollId, title string, multiple bool, options []*Option) error
	AddOptions(pollId PollId, option []*Option) error
}

type voteBot struct {
	api           *tgbotapi.BotAPI
	service       VoteBotService
	nextCommands  map[ChatId]string
	pollTitles    map[ChatId]string
	pollMultiples map[ChatId]bool
	chatUser      map[ChatId]UserId
}

func NewVoteBot(api *tgbotapi.BotAPI, service VoteBotService) *voteBot {
	return &voteBot{
		api,
		service,
		make(map[ChatId]string),
		make(map[ChatId]string),
		make(map[ChatId]bool),
		make(map[ChatId]UserId),
	}
}

func (b *voteBot) Run() error {
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates := b.api.GetUpdatesChan(u)

	var err error
	for update := range updates {
		if err = b.handleUpdate(update); err != nil {
			return err
		}
	}

	return nil
}

func (b *voteBot) handleUpdate(update tgbotapi.Update) error {
	if update.Message == nil { // ignore any non-Message updates
		return nil
	}

	chatId := ChatId(update.Message.Chat.ID)
	text := update.Message.Text

	if update.Message.Command() == "help" {
		if err := b.sendMessage(int64(chatId), "need help?"); err != nil {
			return fmt.Errorf("cannot send message to chat: %w", err)
		}
		return nil
	}

	if update.Message.Command() == "start" {
		userName := update.SentFrom().UserName
		user, err := b.service.CreateUser(userName)
		if err != nil {
			return fmt.Errorf("cannot create user for new user: %w", err)
		}
		b.chatUser[chatId] = user.Id()
		if err := b.sendMessage(int64(chatId), fmt.Sprintf("you registred as\"%s\"", userName)); err != nil {
			return fmt.Errorf("cannot send message to chat: %w", err)
		}
		return nil
	}

	if update.Message.Command() == "poll" {
		if _, ok := b.chatUser[chatId]; !ok {
			if err := b.sendMessage(int64(chatId), "you're not registred, enter start command"); err != nil {
				return fmt.Errorf("cannot send message to chat: %w", err)
			}
			return nil
		}

		if err := b.sendMessage(int64(chatId), "enter poll title"); err != nil {
			return fmt.Errorf("cannot send message to chat: %w", err)
		}
		b.nextCommands[chatId] = "poll_title"
		return nil
	}

	if update.Message.Command() == "polls" {
		if _, ok := b.chatUser[chatId]; !ok {
			if err := b.sendMessage(int64(chatId), "you're not registred, enter start command"); err != nil {
				return fmt.Errorf("cannot send message to chat: %w", err)
			}
			return nil
		}

		userId, ok := b.chatUser[chatId]
		if !ok {
			if err := b.sendMessage(int64(chatId), "you're not registred, enter start command"); err != nil {
				return fmt.Errorf("cannot send message to chat: %w", err)
			}
			return nil
		}
		polls, err := b.service.GetPolls(userId)
		if err != nil {
			if err := b.sendMessage(int64(chatId), "cannot find any poll"); err != nil {
				return fmt.Errorf("cannot send message to chat: %w", err)
			}
			return nil
		}

		pollTitles := make([]string, 0, len(polls))
		for _, p := range polls {
			pollTitles = append(pollTitles, p.Title())
		}

		if err := b.sendMessage(int64(chatId), "found polls: "+strings.Join(pollTitles, ", ")); err != nil {
			return fmt.Errorf("cannot send message to chat: %w", err)
		}
		return nil
	}

	nextCommand, _ := b.nextCommands[chatId]

	if nextCommand == "poll_title" {
		if _, ok := b.chatUser[chatId]; !ok {
			if err := b.sendMessage(int64(chatId), "you're not registred, enter start command"); err != nil {
				return fmt.Errorf("cannot send message to chat: %w", err)
			}
			return nil
		}

		b.pollTitles[chatId] = text
		b.nextCommands[chatId] = "poll_multiple"
		if err := b.sendMessage(int64(chatId), "will it be multiple?"); err != nil {
			return fmt.Errorf("cannot send message to chat: %w", err)
		}
		return nil
	}

	if nextCommand == "poll_multiple" {
		if _, ok := b.chatUser[chatId]; !ok {
			if err := b.sendMessage(int64(chatId), "you're not registred, enter start command"); err != nil {
				return fmt.Errorf("cannot send message to chat: %w", err)
			}
			return nil
		}

		b.pollMultiples[chatId] = text == "true" || text == "1" || text == "yes" || text == "y"
		b.nextCommands[chatId] = "poll_options"
		if err := b.sendMessage(int64(chatId), "enter options (each on new line)"); err != nil {
			return fmt.Errorf("cannot send message to chat: %w", err)
		}
		return nil
	}

	if nextCommand == "poll_options" {
		options := strings.Split(text, "\n")
		if len(options) < 2 {
			if err := b.sendMessage(int64(chatId), "enter at least two options"); err != nil {
				return fmt.Errorf("cannot send message to chat: %w", err)
			}
			return nil
		}
		log.Println(options)

		userId, ok := b.chatUser[chatId]
		if !ok {
			if err := b.sendMessage(int64(chatId), "you're not registred, enter start command"); err != nil {
				return fmt.Errorf("cannot send message to chat: %w", err)
			}
			return nil
		}

		ops := make([]*Option, 0, len(options))
		for _, option := range options {
			ops = append(ops, &Option{option})
		}

		poll, err := b.service.CreatePoll(userId, b.pollTitles[chatId], b.pollMultiples[chatId], ops)
		if err != nil {
			if err := b.sendMessage(int64(chatId), "cannot save poll"); err != nil {
				return fmt.Errorf("cannot send message to chat: %w", err)
			}
			return nil
		}

		if err := b.sendMessage(int64(chatId), "saved new poll: "+poll.Title()); err != nil {
			return fmt.Errorf("cannot send message to chat: %w", err)
		}
		return nil
	}

	if err := b.sendMessage(int64(chatId), "don't understand("); err != nil {
		return fmt.Errorf("cannot send message to chat: %w", err)
	}
	return nil
}

func (b *voteBot) sendMessage(chatId int64, text string) error {
	var err error
	_, err = b.api.Send(tgbotapi.NewMessage(chatId, text))
	if err != nil {
		return fmt.Errorf("cannot send message to telegram: %w", err)
	}
	return nil
}
