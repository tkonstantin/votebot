package internal

import (
	"fmt"
	"github.com/google/uuid"
)

func newId() (string, error) {
	newUUID, err := uuid.NewRandom()
	if err != nil {
		return "", fmt.Errorf("can't generate new uuid: %w", err)
	}
	return newUUID.String(), nil
}
