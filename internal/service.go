package internal

import (
	"fmt"
)

type UserFactory interface {
	CreateUser(name string) (*User, error)
}

type UserRepository interface {
	Add(user *User) error
	Get(id UserId) (*User, error)
}

type PollFactory interface {
	CreatePoll(userId UserId, title string, multiple bool, options []*Option) (*Poll, error)
}

type PollRepository interface {
	Add(poll *Poll) error
	Get(id PollId) (*Poll, error)
	Update(id PollId, title string, multiple bool, options []*Option) error
	GetUserPolls(userId UserId) ([]*Poll, error)
}

type VoteFactory interface {
	CreateVote(userId UserId, pollId PollId, optionsIds []uint8) (*Vote, error)
}

type VoteRepository interface {
	Add(vote *Vote) error
	Get(voteId VoteId) (*Vote, error)
	GetPollVotes(pollId PollId) ([]*Vote, error)
}

type memoryBotService struct {
	userFactory    UserFactory
	userRepository UserRepository
	pollFactory    PollFactory
	pollRepository PollRepository
	voteFactory    VoteFactory
	voteRepository VoteRepository
}

func NewVoteBotService(
	userFactory UserFactory,
	userRepository UserRepository,
	pollFactory PollFactory,
	pollRepository PollRepository,
	voteFactory VoteFactory,
	voteRepository VoteRepository,
) VoteBotService {
	return &memoryBotService{
		userFactory,
		userRepository,
		pollFactory,
		pollRepository,
		voteFactory,
		voteRepository,
	}
}

func (s *memoryBotService) CreateUser(name string) (*User, error) {
	user, err := s.userFactory.CreateUser(name)
	if err != nil {
		return nil, fmt.Errorf("cannot create new user: %w", err)
	}

	err = s.userRepository.Add(user)
	if err != nil {
		return nil, fmt.Errorf("cannot save user: %w", err)
	}

	return user, nil
}

func (s *memoryBotService) CreatePoll(userId UserId, title string, multiple bool, options []*Option) (*Poll, error) {
	poll, err := s.pollFactory.CreatePoll(userId, title, multiple, options)
	if err != nil {
		return nil, fmt.Errorf("cannot create new poll: %w", err)
	}

	err = s.pollRepository.Add(poll)
	if err != nil {
		return nil, fmt.Errorf("cannot save created poll: %w", err)
	}

	return poll, nil
}

func (s *memoryBotService) GetPolls(userId UserId) ([]*Poll, error) {
	polls, err := s.pollRepository.GetUserPolls(userId)
	if err != nil {
		return nil, fmt.Errorf("cannot get user polls: %w", err)
	}

	return polls, nil
}

func (s *memoryBotService) UpdatePoll(pollId PollId, title string, multiple bool, options []*Option) error {
	err := s.pollRepository.Update(pollId, title, multiple, options)
	if err != nil {
		return fmt.Errorf("cannot update poll's options: %w", err)
	}
	return nil
}

func (s *memoryBotService) AddOptions(pollId PollId, option []*Option) error {
	poll, err := s.pollRepository.Get(pollId)
	if err != nil {
		return fmt.Errorf("cannot get poll with this id: %w", err)
	}

	poll.options = append(poll.options, option...)

	err = s.pollRepository.Update(pollId, poll.Title(), poll.Multiple(), poll.Options())
	if err != nil {
		return fmt.Errorf("cannot update poll's options: %w", err)
	}
	return nil
}

type memoryApiService struct {
	pollRepository PollRepository
	voteRepository VoteRepository
	userRepository UserRepository
	userFactory    UserFactory
}

func (m *memoryApiService) RegisterUser(username string) (*User, error) {
	user, err := m.userFactory.CreateUser(username)
	if err != nil {
		return nil, fmt.Errorf("cannot create new user: %w", err)
	}
	err = m.userRepository.Add(user)
	if err != nil {
		return nil, fmt.Errorf("cannot save new user: %w", err)
	}
	return user, nil
}

func (m *memoryApiService) FindUser(id UserId) (*User, error) {
	user, err := m.userRepository.Get(id)
	if err != nil {
		return nil, fmt.Errorf("cannot find user by id: %w", err)
	}
	return user, nil
}

func (m *memoryApiService) FindPoll(id PollId) (*Poll, error) {
	poll, err := m.pollRepository.Get(id)
	if err != nil {
		return nil, fmt.Errorf("cannot find poll with id: %w", err)
	}
	return poll, nil
}

func (m *memoryApiService) FindVotes(id PollId) ([]*Vote, error) {
	votes, err := m.voteRepository.GetPollVotes(id)
	if err != nil {
		return nil, fmt.Errorf("cannot find poll votes: %w", err)
	}
	return votes, nil
}

func (m *memoryApiService) Vote(vote *Vote) error {
	if err := m.voteRepository.Add(vote); err != nil {
		return fmt.Errorf("cannot add new vote: %w", err)
	}
	return nil
}

func NewVoteApiService(
	pollRepository PollRepository,
	voteRepository VoteRepository,
	userRepository UserRepository,
	userFactory UserFactory,
) ApiVoteServer {
	return &memoryApiService{
		pollRepository,
		voteRepository,
		userRepository,
		userFactory,
	}
}
