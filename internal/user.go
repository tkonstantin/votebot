package internal

import (
	"encoding/json"
	"errors"
	"fmt"
	"sync"
)

type UserId string

type User struct {
	id   UserId
	name string
}

func (u *User) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]interface{}{
		"id":   u.id,
		"name": u.name,
	})
}

func (u *User) Id() UserId {
	return u.id
}

func (u *User) Name() string {
	return u.name
}

func newUser(id UserId, name string) *User {
	return &User{
		id,
		name,
	}
}

type uuidUserFactory struct{}

func NewMemoryUserFactory() UserFactory {
	return &uuidUserFactory{}
}

func (f *uuidUserFactory) CreateUser(name string) (*User, error) {
	id, err := newId()
	if err != nil {
		return nil, fmt.Errorf("can't generate new UserId: %w", err)
	}

	u := newUser(UserId(id), name)
	return u, nil
}

type memoryUserRepository struct {
	rwMutex sync.RWMutex
	users   map[UserId]*User
}

func NewMemoryUserRepository() UserRepository {
	return &memoryUserRepository{users: make(map[UserId]*User)}
}

func (r *memoryUserRepository) Add(user *User) error {
	r.rwMutex.Lock()
	defer r.rwMutex.Unlock()

	if user.id == "" {
		return errors.New("user's id cannot be empty")
	}

	if _, ok := r.users[user.id]; ok {
		return errors.New("user with this id already exists")
	}
	r.users[user.id] = user
	return nil
}

func (r *memoryUserRepository) Get(id UserId) (*User, error) {
	r.rwMutex.RLock()
	defer r.rwMutex.RUnlock()

	if id == "" {
		return nil, errors.New("user's id cannot be empty")
	}

	user, ok := r.users[id]
	if !ok {
		return nil, errors.New("cannot find user with this id")
	}

	return user, nil
}
