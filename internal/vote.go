package internal

import (
	"errors"
	"fmt"
	"sync"
)

type VoteId string

type Vote struct {
	id        VoteId
	userId    UserId
	pollId    PollId
	optionIds []uint8
}

func newVote(id VoteId, userId UserId, pollId PollId, options []uint8) *Vote {
	return &Vote{
		id,
		userId,
		pollId,
		options,
	}
}

func (v *Vote) Id() VoteId {
	return v.id
}

func (v *Vote) UserId() UserId {
	return v.userId
}

func (v *Vote) PollId() PollId {
	return v.pollId
}

func (v *Vote) OptionIds() []uint8 {
	return v.optionIds
}

type uuidVoteFactory struct{}

func NewMemoryVoteFactory() VoteFactory {
	return &uuidVoteFactory{}
}

func (f *uuidVoteFactory) CreateVote(userId UserId, pollId PollId, optionsIds []uint8) (*Vote, error) {
	id, err := newId()
	if err != nil {
		return nil, fmt.Errorf("can't generate new UserId: %w", err)
	}

	p := newVote(VoteId(id), userId, pollId, optionsIds)
	return p, nil
}

type memoryVoteRepository struct {
	rmMutex   sync.RWMutex
	votes     map[VoteId]*Vote
	pollVotes map[PollId][]VoteId
}

func NewMemoryVoteRepository() VoteRepository {
	return &memoryVoteRepository{
		votes:     make(map[VoteId]*Vote),
		pollVotes: make(map[PollId][]VoteId),
	}
}

func (m *memoryVoteRepository) Add(vote *Vote) error {
	m.rmMutex.Lock()
	defer m.rmMutex.Unlock()

	if _, ok := m.votes[vote.Id()]; ok {
		return errors.New("vote with this id already exists")
	}
	m.votes[vote.Id()] = vote

	pollVotes, ok := m.pollVotes[vote.PollId()]
	if !ok {
		pollVotes = []VoteId{}
	}
	pollVotes = append(pollVotes, vote.Id())
	m.pollVotes[vote.PollId()] = pollVotes

	return nil
}

func (m *memoryVoteRepository) Get(voteId VoteId) (*Vote, error) {
	if vote, ok := m.votes[voteId]; ok {
		return vote, nil
	}
	return nil, errors.New("cannot find vote with this id")
}

func (m *memoryVoteRepository) GetPollVotes(pollId PollId) ([]*Vote, error) {
	var votes []*Vote

	voteIds, ok := m.pollVotes[pollId]
	if !ok {
		return votes, nil
	}

	for _, voteId := range voteIds {
		vote, err := m.Get(voteId)
		if err != nil {
			return nil, errors.New("cannot find vote with this id")
		}
		votes = append(votes, vote)
	}

	return votes, nil
}
