package internal

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type ApiVoteServer interface {
	RegisterUser(username string) (*User, error)
	FindUser(id UserId) (*User, error)
	FindPoll(id PollId) (*Poll, error)
	FindVotes(id PollId) ([]*Vote, error)
	Vote(vote *Vote) error
}

type server struct {
	*http.ServeMux

	voteServer ApiVoteServer
}

func NewVoteServer(voteServer ApiVoteServer) *server {
	mux := http.NewServeMux()

	s := &server{
		ServeMux:   mux,
		voteServer: voteServer,
	}

	s.setupRoutes()

	return s
}

func (s *server) setupRoutes() {
	s.HandleFunc("/ping", func(writer http.ResponseWriter, request *http.Request) {
		if request.Method != http.MethodGet {
			writer.WriteHeader(http.StatusMethodNotAllowed)
			return
		}

		_, err := writer.Write([]byte("pong"))
		if err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
			return
		}
		writer.WriteHeader(http.StatusOK)
	})

	s.HandleFunc("/user", func(writer http.ResponseWriter, request *http.Request) {
		if request.Method != http.MethodGet {
			writer.WriteHeader(http.StatusMethodNotAllowed)
			return
		}

		userId := request.URL.Query().Get("id")
		if userId == "" {
			writer.WriteHeader(http.StatusBadRequest)
			return
		}

		user, err := s.voteServer.FindUser(UserId(userId))
		if err != nil {
			writer.WriteHeader(http.StatusNotFound)
			return
		}

		writer.WriteHeader(http.StatusOK)
		if err = writeJson(writer, user); err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
			return
		}
	})

	s.HandleFunc("/register", func(writer http.ResponseWriter, request *http.Request) {
		if request.Method != http.MethodPost {
			writer.WriteHeader(http.StatusMethodNotAllowed)
			return
		}

		if err := request.ParseForm(); err != nil {
			writer.WriteHeader(http.StatusBadRequest)
			return
		}

		username := request.Form.Get("username")
		if username == "" {
			writer.WriteHeader(http.StatusBadRequest)
			return
		}

		user, err := s.voteServer.RegisterUser(username)
		if err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
			return
		}

		writer.WriteHeader(http.StatusOK)
		if err = writeJson(writer, struct {
			UserId string `json:"user_id"`
		}{
			string(user.Id()),
		}); err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
			return
		}
	})
}

func writeJson(writer http.ResponseWriter, data interface{}) error {
	jsonBytes, err := json.Marshal(data)
	if err != nil {
		return fmt.Errorf("cannot marshal data to json: %w", err)
	}
	_, err = writer.Write(jsonBytes)
	if err != nil {
		return fmt.Errorf("cannot send data to client: %w", err)
	}
	writer.Header().Set("Content-Type", "application/json")
	return nil
}
